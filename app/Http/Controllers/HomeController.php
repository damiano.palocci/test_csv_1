<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRequest;

class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
    }
    
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function index()
    {
        return view('welcome');
    }

    public function show(){
        $clients = Client::get();
        return view('clients', compact('clients'));
    }
    
    public function store(StoreRequest $request)
    {
        if($request->hasFile('csv') && $request->file('csv')->isValid())
        {
            $csv = $request->file('csv');
            $originalName = $csv->getClientOriginalName(); //CONTAQ_OUTBOUND_33_20201026.csv
            $result = self::checkFileName($originalName);
            if($result['error'] == false){
                unset($result);
                $result = ['error' => false];
                $urlFile = $csv->store('public/csv');
                $fileLines = self::readCSV(storage_path() . '/app/' . $urlFile, ['delimiter' => ';']);
                array_shift($fileLines); //salto la riga 1 con i titoli dei campi 
                for ($i=0; $i < count($fileLines); $i++) { 
                    $result[$i] = $this->validateRow($fileLines[$i]);
                    $contact = new Client();
                    $contact->nome = $fileLines[$i][1];
                    $contact->cognome = $fileLines[$i][2];
                    $contact->email = $fileLines[$i][3];
                    $contact->note = $fileLines[$i][4];
                    $contact->save();

                }
                /* $i = 0;
                foreach($fileLines as $line){
                    $result[$i] = $this->validateRow($line);
                    $contact = new Client();
                    $contact->nome = $line[1];
                    $contact->cognome = $line[2];
                    $contact->email = $line[3];
                    $contact->note = $line[4];
                    $contact->save();
                    $i++;
                } */
                
                return redirect()->route('home')->with('message', $result);
            }
            else{
                return redirect()->route('home')->with('message', $result);

            }
        }
    }
    
    public static function checkFileName($fileName){
        $nameElements = explode('_', $fileName);
        $return = ['error' => false];
        if(count($nameElements) == 4){
            if(!is_string($nameElements[0])){
                $return['error'] = true;
                $return[] = 'Il primo elemento non è una stringa';
            }
            if(!is_string($nameElements[1])){
                $return['error'] = true;
                $return[] = 'Il secondo elemento non è una stringa';
                
            }
            if(!is_numeric($nameElements[2])){
                $return['error'] = true;
                $return[] = 'Il terzo elemento non è un intero';
            }
            $dateElem = explode('.', $nameElements[3]);
            if(strtotime($dateElem[0]) == false){
                $return['error'] = true;
                $return[] = 'Il quarto elemento non è una data';
            }
        }
        else{
            $return['error'] = true;
            $return[] = 'Nome del File: non accettato';
        }
        return $return;
    }
    
    public static function readCSV($csvFile, $arrayDelimiter)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $arrayDelimiter['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }

    public function validateRow($row){
        $return = [];
        if($row[1] == '')
            $return['nome'] = 'Il campo nome è vuoto';
        if($row[2] == '')
            $return['cognome'] = 'Il campo cognome è vuoto';
        if(!filter_var($row[3], FILTER_VALIDATE_EMAIL))
            $return['email'] = 'Il campo email non è valido';
        if($row[4] == '')
            $return['note'] = 'Il campo note è vuoto';
        return $return;
    }
}

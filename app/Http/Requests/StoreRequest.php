<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'csv' => 'required|max:1024|mimes:csv,txt',
        ];
    }

    public function messages(){
        return [
            'csv.required' => 'Devi selezionare un file prima di inviare il form',
            'csv.max' => 'Il file deve essere massimo di 1024KB',
            'csv.mimes' => 'È possibile importare solo file csv o txt',

        ];
    }
}

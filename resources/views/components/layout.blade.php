<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
       <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <title>Document</title>
</head>
<body>
    <x-navbar />
    
    {{$slot}}

    <x-footer />
</body>
</html>
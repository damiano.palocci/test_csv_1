<footer id="sticky-footer" class="py-4 bg-dark text-light ">
    <div class="container text-center">
      <div class="row">
        <div class="col-12 ml-0 pl-0 col-md-2">
          <a class="" href="/"><img src="/images/logo.png" height="40" alt=""></a>
      </div>
        <div class="col-12 col-md-5">
          <p class="text-justify">presto.it è fichissimo perche Lorem ipsum dolor sit amet consectetur adipisicing elit. In odio, inventore laborum, laudantium aspernatur qui ullam eaque molestiae illo voluptate aut, quo sed ex temporibus culpa distinctio velit optio quaerat.</p>
        </div>
        <div class="col-12 col-md-5">
          in nostri contatti:
          <ul class="list-unstyled">
            <li >categorie_fatte_male@mail.com</li>
            <li > tel: 3400000000</li>
            <li >facebook</li>
          </ul>
        </div>
      </div>
      <small> Copyright &copy; presto.it !</small>
      <small>P.IVA 1234567890</small>
    </div>
  </footer>
<x-layout>

    
    @if (session()->has('message') && session()->get('message')['error'])
    <div class="container-fluid">
        <div class="row">
            <div class="alert alert-danger">
                <p>Errore di caricamento del file<br>
                <ul>
                @for ($i = 0; $i < count(session()->get('message'))-1; $i++)

                    <li>{{(session()->get('message'))[$i]}}</li>
                           
                @endfor
                </ul>
                </p>
            </div>
        </div>
    </div>
    @endif

    @if (session()->has('message') && session()->get('message')['error'] == false)
    <div class="container-fluid">
        <div class="row">
            <div class="alert alert-success">
                <p>Risultato dell'importazione</p>
                @for ($i = 0; $i < count(session()->get('message'))-1; $i++)
                <p>
                    Riga: {{$i+1}}<br>
                    <pre>
                    {{print_r((session()->get('message'))[$i], true)}}
                    </pre>
                </p>
                @endfor
            </div>
        </div>
    </div>
    @endif

    <div class="container-fluid vh-100">
        <div class="row no-gutters">
            <div class="col-12">
                <form method="POST" action="{{route('file.store')}}" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                        <label for="exampleFormControlFile1">inserisci file csv</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="csv" >
                        @error('csv')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" class="btn btn-success my-5 " >Salva File</button>
                    </div>
                </form>
                
            </div>
        </div>
    </div>

</x-layout>
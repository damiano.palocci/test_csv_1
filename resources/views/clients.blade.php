<x-layout>
<div class="container">
    <div class="row justify-content-center">
        @forelse ($clients as $client)
        <div class="col-8">
            <div class="card mb-2">
                <div class="card-body">
                    <div class="card-title">Cliente ID: {{$client->id}}</div>
                    <div class="card-text">Nome e Cognome: {{$client->nome}} {{$client->cognome}}</div>
                    <div class="card-text">Email: {{$client->email}}</div>
                    <div class="card-text">Note: {{$client->note}}</div>
                </div>
            </div>
        </div>
        @empty
        <div class="col-12">
            <div class="alert alert-secondary text-center" role="alert">
              <h3>Nessun cliente importato!</h3>
            </div>
          </div>
        @endforelse
    </div>
</div>
</x-layout>
